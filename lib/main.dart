import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Calculado de IMC",
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _info = "Informe seus dados";


  void _resetFields(){
    weightController.text = "";
    heightController.text = "";

    setState(() {
      _info = "Informe seus dados";
      _formKey = GlobalKey<FormState>();
    });
  }

  void _calculate (){
    setState(() {
      double weight= double.parse(weightController.text);
      double height= double.parse(heightController.text) / 100;

      double imc = weight / (height * height);

      print(imc);

      if (imc < 18.6 ){
        _info = "Abaixo do peso (${imc.toStringAsPrecision(2)})";
      } else if(imc >= 18.6 && imc < 24.9){
        _info = "Peso ideal (${imc.toStringAsPrecision(2)})";
      } else if(imc >= 24.9 && imc < 29.9){
        _info = "Levemente acima do peso (${imc.toStringAsPrecision(2)})";
      } else if(imc >= 29.9 && imc < 34.9){
        _info = "Obesidade grau I (${imc.toStringAsPrecision(2)})";
      } else if(imc >= 34.0 && imc < 39.9){
        _info = "Obesidade grau II (${imc.toStringAsPrecision(2)})";
      } else if(imc >40){
        _info = "Obesidade grau III (${imc.toStringAsPrecision(2)})";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculadora de IMC"),
        centerTitle: true,
        backgroundColor: Colors.deepOrangeAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _resetFields,
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Icon(Icons.person, size: 120, color: Colors.deepOrangeAccent),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: "Peso (kg)",
                    labelStyle: TextStyle(color: Colors.deepOrangeAccent)),
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.deepOrangeAccent, fontSize: 25),
                controller: weightController,
                validator: (value){
                  if(value.isEmpty){
                    return "Insira seu peso";
                  } else{
                    return null;
                  }
                },
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: "Altura (cm)",
                    labelStyle: TextStyle(color: Colors.deepOrangeAccent)),
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.deepOrangeAccent, fontSize: 25),
                controller: heightController,
                validator: (value){
                  if(value.isEmpty){
                    return "Insira sua Altura";
                  } else{
                    return null;
                  }
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 15, bottom: 10),
                child: Container(
                  height: 50,
                  child: RaisedButton(
                    onPressed: () {
                      if(_formKey.currentState.validate()){
                        _calculate();
                      }
                    },
                    child: Text(
                      "Calcular",
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                    color: Colors.deepOrangeAccent,
                  ),
                ),
              ),
              Text(
                _info,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.deepOrangeAccent, fontSize: 25),
              )
            ],
          ),
        )
      )
    );
  }
}
